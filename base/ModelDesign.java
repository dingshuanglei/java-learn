abstract class Action{
    public static final int EAT=1;
    public static final int SLEEP=5;
    public static final int WORK=10;
    public void command(int code){
        switch (code) {
            case EAT:{
                this.eat();
                break;
            }
            case SLEEP:{
                this.sleep();
                break;
            }
            case WORK:{
                this.work();
                break;
            }
            case EAT + SLEEP + WORK :{
                this.eat();
                this.sleep();
                this.work();
                break;
            }
        }
    }

    public abstract void eat();

    public abstract void sleep();

    public abstract void work();

}
class Robot extends Action{
    public void eat(){
        System.out.println("充电");
    }
    public void sleep(){}
    public void work(){
        System.out.println("一直工作");
    }
}
class Person extends Action{
    public void eat(){
        System.out.println("吃饭");
    }
    public void sleep(){
        System.out.println("躺床上睡觉");
    }
    public void work(){
        System.out.println("挣钱工作");
    }
}
class Pig extends Action{
    public void eat(){
        System.out.println("专用食物");
    }
    public void sleep(){
        System.out.println("躺地上睡觉");
    }
    public void work(){
    }
}

public class ModelDesign {
	public static void main(String[] args) {
        Action robot=new Robot();
        System.out.println("-----机器-----");
        robot.command(Action.EAT);
        robot.command(Action.WORK);
        System.out.println("-----人类-----");
        Action person=new Person();
        person.command(Action.SLEEP+Action.EAT+Action.WORK);
        System.out.println("-----猪猪-----");   
        Action pig=new Pig();
        pig.work();
        pig.eat();
    }
}