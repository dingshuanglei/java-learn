public class ArrayDemo{
    public static void main(String args[]){
        int data [] = new int []{8,9,0,2,3,5,10,7,6,1};
        ArrayUtil.sort(data) ;
        ArrayUtil.printArray(data);
    }
}

class ArrayUtil{
    public static void printArray(int data[]){
        for(int x = 0; x < data.length; x++){
            if (x < data.length -1){
                System.out.print(data[x]+"、");
            }
            else{
                System.out.print(data[x]);
            }
        }
    }

    public static void sort(int data[]){
        int count=0;
        for (int x = 0; x < data.length; x++){
            System.out.println("******第"+(x+1)+"轮排序开始******");
            int singleCount = 0;
            for(int y=0; y < data.length - x -1; y++){
                // System.out.println("******第"+(x+1)+":"+(y+1)+"次排序开始******");
                if (data[y] > data[y+1]){
                    int temp = data[y];
                    data[y] = data[y+1];
                    data[y+1] = temp;
                    System.out.println("本次排序交换："+data[y]+":"+data[y+1]+" to "+data[y+1]+"->"+data[y]);
                }
                // System.out.println("******第"+(x+1)+":"+(y+1)+"次排序结束******");
                count +=1;
                singleCount +=1;
            }
            System.out.println("******本次排序次数"+singleCount+"******");
        }
        System.out.println("******一共"+count+"次排序结束******");
    }
}