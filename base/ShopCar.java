interface ILink<E>{//使用泛型避免安全隐患
	
    public void add(E e);//增加数据
	
	public int size();//保存数据个数
	
	public boolean isEmpty();//判断是否空集合
	
	public Object [] toArrary();//返回
	
	public E get(int index);//根据索引获取数据
	
	public void set(int index,E data);//修改索引数据

	public boolean contains(E data);//判断数据是否存在

	public void remove(E data);//删除数据

	public void clear();//清空集合
}
class LinkImpl<E> implements ILink<E>{
    private class Node {//保存节点的数据关系
        private E data;//保存数据
		private Node next;//保存下一个引用
		public Node(E data){//有数据情况才有意义
			this.data=data;
		}
		//第一次调用LinkImpl.root;
		//第二次调用LinkImpl.root.next;
		//第三次调用LinkImpl.root.next.next;
		public void addNode(Node newNode)//保存新的Node数据
		{
			if(this.next==null){//当前节点的下一个节点为null
				this.next=newNode;//保存当前节点
			}else{
				this.next.addNode(newNode);
			}
		}
		//第一次调用LinkImpl.root;
		//第二次调用LinkImpl.root.next;
		//第三次调用LinkImpl.root.next.next;
		public void toArraryNode(){
			LinkImpl.this.returnData[LinkImpl.this.foot++]=this.data;
			if(this.next!=null)
			{
				this.next.toArraryNode();
			}
		}
		
		public E getNode(int index){
			if(LinkImpl.this.foot++==index){//索引相同
				return this.data;
			}else{
				return this.next.getNode(index);
			}
		}
		
		public void setNode(int index,E data){
			if(LinkImpl.this.foot++==index){//索引相同
				this.data=data;
			}else{
				this.next.setNode(index,data);
			}
		}

		public boolean containsNode(E data){
			//对象比较
			if(this.data.equals(data)){
				return true;
			}else{
				if(this.next==null){//最后一个节点了
					return false;
				}
				else{
					//递归后续比较
					return this.next.containsNode(data);
				}
			}
		}

		public void removeNode(Node previous,E data){
			if(this.data.equals(data)){
				previous.next=this.next;//空出当前节点
			}
			else{
				if(this.next!=null)//有后续节点
				{
					this.next.removeNode(this,data);//向后继续删除
				}
			}
		}
		
    }
	//---------link定义成员
	private Node root;//保存根元素
	private int count;//保存数据个数
	private int foot;//操作数组脚标
	private Object [] returnData;//返回数据保存
	//---------link定义方法
	public void add(E e){
		if(e == null){
			return;//数据空值，方法结束
		}
		//数据本身不具有关联特性的，只有Node类有，那么要想实现关联处理就需要将数据包装在Node中
		Node newNode = new Node(e);//创建一个节点
		if(this.root==null){//现在没有根节点
			this.root=newNode;//第一个节点作为根节点
		}else{//根节点存在
			this.root.addNode(newNode);//保存到合适位置
		}
		this.count++;
	}
	public int size(){
		return this.count;
	}
	public boolean isEmpty(){
		return this.count<=0;
	}
	public Object[] toArrary(){
		if(this.isEmpty())
		{
			//空集合
			return null;
		}
		this.foot=0;//脚标清零
		this.returnData = new Object[this.count];//根据已有长度开辟空间
		this.root.toArraryNode();//利用Node类进行递归数据获取
		return this.returnData;
	}
	
	public E get(int index){
		if(index>=this.count){
			//索引超出范围
			return null;
		}
		this.foot=0;//重置索引下标
		return this.root.getNode(index);
	}
	
	public void set(int index,E data){
		if(index>=this.count){
			//索引超出范围
			return;
		}
		this.foot=0;//重置索引下标
		this.root.setNode(index,data);
	}
	

	public boolean contains(E data)
	{
		if(data==null){
			// 空数据
			return false;
		}
		// 判断是否存在
		return this.root.containsNode(data);
	}

	public void remove(E data)
	{
		if(this.contains(data)){
			// 判断根节点是否删除节点
			if(this.root.data.equals(data)){
				// 根节点指向下一个
				this.root=this.root.next;
			}
			else{//交给Node类负责删除
				this.root.next.removeNode(this.root,data);
			}
			this.count--;
		}
	}
	public void clear()
	{
		this.root=null;//根节点置空，其余节点全部消失
		this.count=0;//数量重置为零
	}
}

interface IGoods{
    public String getName();

    public double getPrice();
}


interface IShopcar{
    public void add(IGoods goods);
    public void delete(IGoods goods);
    public Object[] getAll();
}

class ShopCarImpl implements IShopcar{
   private ILink<IGoods>allGoods=new LinkImpl<IGoods>();
   public void add(IGoods goods){
       this.allGoods.add(goods);
    }
    public void delete(IGoods goods){
       this.allGoods.remove(goods);
    }
    public Object[] getAll(){
        return this.allGoods.toArrary();
    }
}

class Cashier{
    private IShopcar shopCar;
    public Cashier(IShopcar shopCar){
        this.shopCar=shopCar;
    }

    public double allPrice(){
        Object result[]=this.shopCar.getAll();
        double all=0.0;
        for(Object obj:result){
            IGoods goods = (IGoods) obj;
            all+= goods.getPrice();
        }
        return all;
    }

    public int allCount(){
        return this.shopCar.getAll().length;
    }

}

class Book implements IGoods{
    private String name;
    private double price;
    public Book(String name,double price){
        this.name=name;
        this.price=price;
    }
    public String getName(){
        return this.name;
    }
    public double getPrice(){
        return this.price;
    }

    public boolean equals(Object obj){
        if(obj==null){
            return false;
        }
        if(this==obj){
            return true;
        }
        if(!(obj instanceof Book)){
            return false;
        }
        Book book=(Book)obj;
        return this.name.equals(book.name)&&this.price==book.price;
    }

    public String toString(){
        return "【图书】名称："+this.name+" 价格："+this.price;
    }
}

class Bag implements IGoods{
    private String name;
    private double price;
    public Bag(String name,double price){
        this.name=name;
        this.price=price;
    }
    public String getName(){
        return this.name;
    }
    public double getPrice(){
        return this.price;
    }

    public boolean equals(Object obj){
        if(obj==null){
            return false;
        }
        if(this==obj){
            return true;
        }
        if(!(obj instanceof Bag)){
            return false;
        }
        Bag bag=(Bag)obj;
        return this.name.equals(bag.name)&&this.price==bag.price;
    }

    public String toString(){
        return "【书包】名称："+this.name+" 价格："+this.price;
    }
}

public class ShopCar {
	public static void main(String[] args) {
        
		IShopcar car=new ShopCarImpl();
        car.add(new Book("java开发",79.8));
        car.add(new Book("oracle",89.8));
        car.add(new Bag("无敌书包",889.8));

        Cashier cashier=new Cashier(car);
        System.out.println("总价格:"+cashier.allPrice()+"数量:"+cashier.allCount());

    }
}