public class ArraySwap{
    public static void main(String args[]) {
        int data [] = new int [] {1,2,3,4,5,6,7,8,9};
        SwapUtil.Swap1(data);
        SwapUtil.PrintArray(data);
        System.out.println("");
        System.out.println("------------------------");
        SwapUtil.Swap2(data);
        SwapUtil.PrintArray(data);
    } 
}

class SwapUtil{
    public static void Swap1(int data[]) {
        int temp[]=new int[data.length];
        int foot=temp.length-1;
        for(int x=0;x<data.length;x++){
            temp[foot--]=data[x];
        }
        data=temp;
    }

    public static void Swap2(int data[]){
        int center = data.length/2;
        int head=0;
        int tail=data.length-1;
        for(int x=0;x<center;x++){
            int temp = data[head];
            data[head]=data[tail];
            data[tail]=temp;
            head++;
            tail--;
        }
    }

    public static void PrintArray(int data[]){
        for(int x = 0; x < data.length; x++){
            if (x < data.length -1){
                System.out.print(data[x]+",");
            }
            else{
                System.out.print(data[x]);
            }
        }
    }
}