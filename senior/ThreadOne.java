// 柔和停止线程
public  class ThreadOne{
    public static boolean flag = true;
    public static void main(String[] args) throws Exception {
        // final boolean flag = true;
        new Thread(()->{
            long num = 0;
            while(flag){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"正在运行num:"+num++);
            }
        },"执行线程").start();
        Thread.sleep(200);
        flag = false;
    }
}