class Computer{
    private static int count=0;
    private String name;
    private double price;
    public Computer(String name,double price){
        this.name=name;
        this.price=price;
        count++;
    }
    public String toString(){
        return "第"+this.count+"台电脑 电脑名称:"+this.name+"价格:"+this.price;
    }
}

class Resource{
    private Computer computer;
    public synchronized void make() throws Exception{
        if(this.computer!=null ){
            super.wait();
        }
        Thread.sleep(100);
        this.computer=new Computer("onlyelf",100);
        System.out.println("【生产电脑】"+this.computer);
        super.notifyAll();
    }
    public synchronized void get() throws Exception{
        if(this.computer==null ){
            super.wait();
        }
        Thread.sleep(10);
        System.out.println(this.computer);
        System.out.println("【取走电脑】"+this.computer);
        this.computer=null;
        super.notifyAll();
    }
}

class Producer implements Runnable{
    private Resource resource;
    public Producer(Resource resource){
        this.resource=resource;
    }
    @Override
    public void run(){
        for(int x=0;x<50;x++){
            try {
                this.resource.make();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer implements Runnable{
    private Resource resource;
    public Consumer(Resource resource){
        this.resource=resource;
    }
    @Override
    public void run(){
        for(int x=0;x<50;x++){
            try {
                this.resource.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

public  class ThreadComputer{
    public static void main(String[] args) throws Exception {
        Resource res=new Resource();
        new Thread(new Producer(res)).start();
        new Thread(new Consumer(res)).start();
    }
}
