class Resource{
    private int num=0;
    private boolean flag=true;//加减切换
    // flag = true 表示加法操作
    // flag = false 表示减法操作
    public synchronized void add() throws Exception{
        if(this.flag==false){
            super.wait();
        }
        Thread.sleep(100);
        this.num++;
        System.out.println("【加法操作 "+Thread.currentThread().getName()+"】num = "+this.num);
        this.flag=false;
        super.notifyAll();
    }
    public synchronized void sub() throws Exception{
        if(this.flag==true){
            super.wait();
        }
        Thread.sleep(200);
        this.num--;
        System.out.println("【减法操作 "+Thread.currentThread().getName()+"】num = "+this.num);
        this.flag=true;
        super.notifyAll();
    }
}

class AddThread implements Runnable{
    private Resource resource;
    public AddThread(Resource resource){
        this.resource=resource;
    }
    @Override
    public void run(){
        for(int x=0;x<50;x++){
            try {
                this.resource.add();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

class SubThread implements Runnable{
    private Resource resource;
    public SubThread(Resource resource){
        this.resource=resource;
    }
    @Override
    public void run(){
        for(int x=0;x<50;x++){
            try {
                this.resource.sub();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}


public class NumberPlusSub{
    public static void main(String[] args) {
        Resource res=new Resource();
        AddThread at=new AddThread(res);
        SubThread st=new SubThread(res);
        new Thread(at,"加法线程-A").start();
        new Thread(at,"加法线程-B").start();
        new Thread(st,"减法线程-X").start();
        new Thread(st,"减法线程-Y").start();
    }
}