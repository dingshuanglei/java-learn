
public class ThreadDemo{
    public static void main(String[] args) throws Exception {
        Message msg = new Message();
        new Thread(new Producer(msg)).start();
        new Thread(new Consumer(msg)).start();
    }
}  

class Producer implements Runnable{
    private Message msg;
    public Producer(Message msg){
        this.msg = msg;
    }

    @Override
    public void run(){
        for(int x=0; x<100; x++){
            if(x % 2 == 0){
                // try {
                //     Thread.sleep(100);
                // } catch (Exception e) {
                //     e.printStackTrace();
                // }
                this.msg.set("小丁","帅哥一枚");
            }else{
                // try {
                //     Thread.sleep(100);
                // } catch (Exception e) {
                //     e.printStackTrace();
                // }
                this.msg.set("小李","妹纸一枚");
            }
        }
    }
}

class Consumer implements Runnable{
    private Message msg;
    public Consumer(Message msg){
        this.msg = msg;
    }
    @Override
    public void run(){
        for(int x=0;x<100;x++){
            // try {
            //     Thread.sleep(10);
            // } catch (Exception e) {
            //     e.printStackTrace();
            // }
            System.out.println(this.msg.get());
        }
    }
}

class Message{
    private String title;
    private String content;
    private boolean flag=true;
    // flag=true 允许生产 不允许消费
    // flag=false 允许消费 不允许生产
    public void setContent(String content){
        this.content=content;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public String getContent(){
        return this.content;
    }
    public String getTitle(){
        return this.title;
    }
    // synchronized
    public synchronized void set(String title, String content){
        if(this.flag==false){
            try {
                super.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.title=title;
        this.content=content;
        this.flag=false;
        super.notify();
    }
    public synchronized String get(){
        if(this.flag==true){
            try {
                super.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return this.title+"-"+this.content;
        } finally{
            this.flag=true;
            super.notify();
        }

    }

}


