import java.util.Arrays;

public class JavaTree{
    public static void main(String[] args) {
        BinaryTree<Person> tree=new BinaryTree<Person>();
        tree.add(new Person("Сǿ-80",80));
        tree.add(new Person("Сǿ-30",30));
        tree.add(new Person("Сǿ-50",50));
        tree.add(new Person("Сǿ-60",60));
        tree.add(new Person("Сǿ-90",90));
        System.out.println(Arrays.toString(tree.toArray()));
        System.out.println("--------------------------------------");
        // BinaryTree<Person> tree2=new BinaryTree<Person>();
        // tree2.add(new Person("Сǿ-80",80));
        // tree2.add(new Person("Сǿ-50",50));
        // tree2.add(new Person("Сǿ-60",60));
        // tree2.add(new Person("Сǿ-30",30));
        // tree2.add(new Person("Сǿ-90",90));
        // tree2.add(new Person("Сǿ-10",10));
        // tree2.add(new Person("Сǿ-55",55));
        // tree2.add(new Person("Сǿ-70",70));
        // tree2.add(new Person("Сǿ-85",85));
        // tree2.add(new Person("Сǿ-95",95));
        // tree2.remove(new Person("Сǿ-10",10));
        // System.out.println(Arrays.toString(tree2.toArray()));
        // System.out.println("--------------------------------------");
        BinaryTree<Person> tree3=new BinaryTree<Person>();
        tree3.add(new Person("Сǿ-80",80));
        tree3.add(new Person("Сǿ-50",50));
        tree3.add(new Person("Сǿ-60",60));
        tree3.add(new Person("Сǿ-30",30));
        tree3.add(new Person("Сǿ-90",90));
        tree3.add(new Person("Сǿ-10",10));
        tree3.add(new Person("Сǿ-55",55));
        tree3.add(new Person("Сǿ-70",70));
        tree3.add(new Person("Сǿ-85",85));
        tree3.add(new Person("Сǿ-95",95));
        tree3.remove(new Person("Сǿ-80",80));
        System.out.println(Arrays.toString(tree3.toArray()));
    }
}

class BinaryTree<T extends Comparable<T>>{
    private class Node{
        private Comparable<T>data;
        private Node parent;
        private Node left;
        private Node right;
        public Node(Comparable<T> data){
            this.data=data;
        }

        public void addNode(Node newNode){
            if(newNode.data.compareTo((T)this.data)<=0){
                if(this.left==null){
                    this.left=newNode;
                    newNode.parent=this;
                }else{
                    this.left.addNode(newNode);
                }
            }else{
                if(this.right==null){
                    this.right=newNode;
                    newNode.parent=this;
                }else{
                    this.right.addNode(newNode);
                }
            }
        }

        public void toArrayNode(){
            if(this.left!=null){
                this.left.toArrayNode();
            }
            BinaryTree.this.returnData[BinaryTree.this.foot++]=this.data;
            if(this.right!=null){
                this.right.toArrayNode();
            }
        }

        public boolean containsNode(Comparable<T>data){
            if(data.compareTo((T)this.data)==0){
                return true;
            }else if(data.compareTo((T)this.data)<0){
                if(this.left!=null){
                    return this.left.containsNode(data);
                }else{
                    return false;
                }
            }else{
                if(this.right!=null){
                    return this.right.containsNode(data);
                }else{
                    return false;
                }
            }
        }

        public Node getRemoveNode(Comparable<T> data){
            if(data.compareTo((T)this.data)==0){
                return this;
            }else if(data.compareTo((T)this.data)<0){
                if(this.left!=null){
                    return this.left.getRemoveNode(data);
                }else{
                    return null;
                }
            }else{
                if(this.right!=null){
                    return this.right.getRemoveNode(data);
                }else{
                    return null;
                }
            }
        }

    }
    // ������ʵ��
    private Node root;//���ڵ�

    private int count;

    private Object[]returnData;

    private int foot=0;

    public void add(Comparable<T> data){
        if(data==null){
            throw new NullPointerException("��������Ϊ��!");
        }
        Node newNode=new Node(data);
        if(this.root==null){
            this.root=newNode;
        }else{
            this.root.addNode(newNode);
        }
        this.count++;
    }

    public Object[] toArray(){
        if(this.count==0){
            return null;
        }
        this.returnData = new Object[this.count];
        this.foot=0;
        this.root.toArrayNode();
        return this.returnData;
    }


    public void remove(Comparable<T>data){
        if(this.root==null){
            return;
        }else{
            if(this.root.data.compareTo((T)data)==0){
                Node moveNode = this.root.right;
                while(moveNode.left!=null){
                    moveNode=moveNode.left;
                }
                moveNode.left=this.root.left;
                moveNode.right=this.root.right;
                moveNode.parent.left=null;
                this.root = moveNode;
            }else{
                Node removeNode = this.root.getRemoveNode(data);
                if(removeNode!=null){
                    if(removeNode.left==null&&removeNode.right==null){
                        removeNode.parent.left=null;
                        removeNode.parent.right=null;
                        removeNode.parent=null;
                    }else if(removeNode.left!=null&&removeNode.right==null){
                        removeNode.parent.left=removeNode.left;
                        removeNode.left.parent=removeNode.parent;
                    }else if(removeNode.left==null&&removeNode.right!=null){
                        removeNode.parent.right=removeNode.right;
                        removeNode.right.parent=removeNode.parent;
                    }else{
                        Node moveNode = removeNode.right;
                        while(moveNode.left!=null){
                            moveNode = moveNode.left;
                        }
                        removeNode.parent.left=moveNode;
                        moveNode.parent.left=null;
                        moveNode.parent=removeNode.parent;
                        moveNode.right=removeNode.right;
                        moveNode.left=removeNode.left;
                    }
                }
            }
            this.count--;
        }
    }
}

class Person implements Comparable<Person>{
    private String name;
    private int age;
    public Person(String name,int age){
        this.name=name;
        this.age=age;
    }
    @Override
    public int compareTo(Person per){
        return this.age - per.age;
    }
    @Override
    public String toString(){
        return "��Person��������"+this.name+" ���䣺"+this.age;
    }
}

