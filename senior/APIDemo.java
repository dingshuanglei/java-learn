interface IMessage extends AutoCloseable{
    public void send();
}

class NetMessage implements IMessage{
    private String msg;
    public NetMessage(String msg){
        this.msg=msg;
    }

    public boolean open(){
        System.out.println("打开消息发送连接资源");
        return true;
    }
    @Override
    public void send(){
        if(this.open()){
            System.out.println("发送消息"+this.msg);
        }
    }
    @Override
    public void close(){
        System.out.println("关闭消息发送连接资源");
    }
}


public class APIDemo {
    public static void main(String[] args) throws Exception {
        try (NetMessage mn=new NetMessage("www.onlyelf.cn")){
            mn.send();
        } catch (Exception e) {
        }
        
        
    }
}