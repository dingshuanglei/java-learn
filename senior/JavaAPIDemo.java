
class Message{
    private String info;
    public void setInfo(String info){
        this.info=info;
    }
    public String getInfo(){
        return this.info;
    }
}

class Channel{
    // private static Message message;
    private static final ThreadLocal<Message>THREADLOCAL=new ThreadLocal<Message>();
    private Channel(){}
    public static void setMessage(Message m){
        THREADLOCAL.set(m);
    }
    public static void send(){
        // System.out.println("【"+Thread.currentThread().getName()+"消息发送】"+message.getInfo());
        System.out.println("【"+Thread.currentThread().getName()+"消息发送】"+THREADLOCAL.get().getInfo());
    }
}


public class JavaAPIDemo {
    public static void main(String[] args) {
        new Thread(()->{
            Message msg=new Message();
            // msg.setInfo("www.onlyelf.cn");
            msg.setInfo("第一条消息");
            Channel.setMessage(msg);
            Channel.send();
        },"消息A").start();
        new Thread(()->{
            Message msg=new Message();
            msg.setInfo("第二条消息");
            Channel.setMessage(msg);
            Channel.send();
        },"消息B").start();
        new Thread(()->{
            Message msg=new Message();
            msg.setInfo("第三条消息");
            Channel.setMessage(msg);
            Channel.send();
        },"消息C").start();
 
    }
}