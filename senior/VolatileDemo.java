class MyThread implements Runnable{
    // private int ticket=5;
    private volatile int ticket=5;
    @Override 
    public void run(){
        synchronized(this){
            while(this.ticket>0){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"��Ʊ���� ticket:"+this.ticket--);
            }
        }
       
    }
}

public class VolatileDemo{
    public static void main(String[] args) {
        MyThread mt=new MyThread();
        new Thread(mt,"Ʊ����A").start();
        new Thread(mt,"Ʊ����B").start();
        new Thread(mt,"Ʊ����C").start();
    }
}